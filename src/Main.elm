module Main exposing (..)

import Browser
import Element exposing (alignBottom, centerX, centerY, column, el, fill, height, html, layout, padding, paddingXY, spacing, text, width)
import Element.Border as Border
import Element.Events
import Html exposing (Html, iframe)
import Html.Attributes exposing (property, src, style)
import Http
import Json.Decode as Decoder exposing (Decoder)
import Json.Encode
import Material.IconButton as IconButton


imgClapperBoardOpen : String
imgClapperBoardOpen =
    "mdi:movie-open-outline"


imgClapperBoardClosed : String
imgClapperBoardClosed =
    "mdi:movie-outline"



-- MAIN


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Status
    = Failure String
    | Loading
    | Loaded


type alias Model =
    { status : Status
    , selectedShortFilm : Maybe ShortFilm
    , surpriseButtonImg : String
    }


initialModel : Model
initialModel =
    Model Loading Nothing imgClapperBoardClosed


init : () -> ( Model, Cmd Msg )
init _ =
    ( initialModel, fetchFilmPage )



-- UPDATE


type Msg
    = GotShortFilm (Result Http.Error ShortFilm)
    | ClickedSurprise
    | HoveredSurprise
    | UnhoveredSurprise


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotShortFilm (Ok shortFilm) ->
            ( { model | status = Loaded, selectedShortFilm = Just shortFilm, surpriseButtonImg = imgClapperBoardOpen }, Cmd.none )

        GotShortFilm (Err _) ->
            ( { model | status = Failure "Error loading film" }, Cmd.none )

        ClickedSurprise ->
            ( { model | status = Loading, surpriseButtonImg = imgClapperBoardClosed }, fetchFilmPage )

        HoveredSurprise ->
            case model.status of
                Failure _ ->
                    ( model, Cmd.none )

                Loading ->
                    ( model, Cmd.none )

                Loaded ->
                    ( { model | surpriseButtonImg = imgClapperBoardClosed }, Cmd.none )

        UnhoveredSurprise ->
            case model.status of
                Failure _ ->
                    ( model, Cmd.none )

                Loading ->
                    ( model, Cmd.none )

                Loaded ->
                    ( { model | surpriseButtonImg = imgClapperBoardOpen }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Html Msg
view model =
    layout
        [ padding 50
        , width fill
        , height fill
        ]
        (column
            [ centerX
            , spacing 20
            , width fill
            , height fill
            ]
            (case model.status of
                Failure text ->
                    [ Element.text ("I could not load a random sotw for some reason. " ++ text) ]

                Loading ->
                    [ viewSpinner
                    , viewSurpriseButton model
                    ]

                Loaded ->
                    [ viewPlayer model
                    , viewSurpriseButton model
                    ]
            )
        )


viewSpinner : Element.Element Msg
viewSpinner =
    el
        [ centerX
        , centerY
        ]
        (Element.html
            (Html.span
                [ Html.Attributes.class "iconify"
                , Html.Attributes.class "spinner"
                , Html.Attributes.attribute "data-icon" "mdi:video-vintage"
                ]
                []
            )
        )


viewPlayer : Model -> Element.Element Msg
viewPlayer model =
    el
        [ centerX
        , paddingXY 250 0
        , width fill -- Container defining the width. Height is defined in child using padding-top with a percentage of 56.25% (represents a 16:9 ratio)
        ]
        (el
            [ width fill
            , Element.htmlAttribute (style "padding-top" "56.25%")
            , Element.htmlAttribute (style "position" "relative")
            ]
            (html
                (iframe
                    [ src
                        (case model.selectedShortFilm of
                            Nothing ->
                                ""

                            Just shortFilm ->
                                shortFilm.filmUrl
                        )
                    , style "border" "none"
                    , style "width" "100%"
                    , style "height" "100%"
                    , style "position" "absolute"
                    , style "top" "0"
                    , style "left" "0"
                    , property "allow" (Json.Encode.string "fullscreen")
                    ]
                    []
                )
            )
        )


viewSurpriseButton : Model -> Element.Element Msg
viewSurpriseButton model =
    el
        [ centerX
        , alignBottom
        , Border.width 1
        , Border.rounded 50
        , Border.color (Element.rgb255 68 75 84)
        , Element.Events.onMouseEnter HoveredSurprise
        , Element.Events.onMouseLeave UnhoveredSurprise
        ]
        (html
            (IconButton.iconButton
                (IconButton.config |> IconButton.setOnClick ClickedSurprise)
                (IconButton.customIcon Html.span
                    [ Html.Attributes.class "iconify"
                    , Html.Attributes.attribute "data-icon" model.surpriseButtonImg
                    , Html.Attributes.style "color" "rgb(68, 75, 84)"
                    ]
                    []
                )
            )
        )


type alias ShortFilm =
    { filmUrl : String }


filmPageDecoder : Decoder ShortFilm
filmPageDecoder =
    Decoder.map ShortFilm (Decoder.field "film_url" Decoder.string)


fetchFilmPage : Cmd Msg
fetchFilmPage =
    Http.get
        { url = "https://shorty-be.azurewebsites.net/api/random"
        , expect = Http.expectJson GotShortFilm filmPageDecoder
        }

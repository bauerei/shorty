| Software | License |
| -------- | ------- |
| [material-components-web-elm](https://package.elm-lang.org/packages/aforemny/material-components-web-elm/7.0.0/) *7.0.0* | [MIT](https://github.com/aforemny/material-components-web-elm/blob/7.0.0/LICENSE) |
| [elm-ui](https://package.elm-lang.org/packages/mdgriffith/elm-ui/1.1.8/) *1.1.8* | [BSD-3-Clause](https://github.com/mdgriffith/elm-ui/blob/1.1.8/LICENSE) |
| [iconify](https://iconify.design/) *2.0.3* | [Apache license 2.0](https://www.apache.org/licenses/LICENSE-2.0) |
| Icons from [Material Design Icons](https://materialdesignicons.com/) | [Apache license 2.0](https://dev.materialdesignicons.com/license) |